
import com.sendyit.common.utils.Config;
import com.sendyit.lens.logs.utils.Markers;
import com.sendyit.positions.model.HiveMQ;
import com.sendyit.positions.store.Postgres;
import db.Database;
import db.postgres.PostgresDB;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.sendyit.positions.store.Postgres.createPostgresConnection;


@QuarkusMain
public class Main {

   // private static final Logger logger = Logger.logger(Main.class);
    private static final Logger logger= LoggerFactory.getLogger(Main.class);
    public  static void main (String [] args){


        Quarkus.run(args);
       logger.info ("Positions App is Starting ...");
        initialize();

    }

    public static void initialize(){
        HiveMQ.createConnection();



    }
}
