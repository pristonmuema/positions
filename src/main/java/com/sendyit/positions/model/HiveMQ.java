package com.sendyit.positions.model;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.exceptions.ConnectionFailedException;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;
import org.json.JSONObject;


import java.util.UUID;

import static com.sendyit.positions.store.Postgres.createPostgresConnection;


public class HiveMQ {

public static Mqtt5AsyncClient mqtt5AsyncClient;

    private static void initHiveMQClients() {
        initMQTTLocationClient();

    }

private static void initMQTTLocationClient(){
        try {
            mqtt5AsyncClient = MqttClient.builder()
                    .useMqttVersion5()
                    .identifier(UUID.randomUUID().toString())
                    .serverHost("localhost")
                    .serverPort(1883)
                    .buildAsync();

        }catch (ConnectionFailedException e){
            e.printStackTrace();
            System.out.println("++++++++++ Exception e Occurred " + e);
        }

    }

    public static void  createConnection(){
        initHiveMQClients();

        mqtt5AsyncClient.connect()

                .whenComplete((connAck, throwable ) ->{
            if (throwable != null){
                throwable.printStackTrace();
                System.out.println("HiveMQ Connection Failed ......." );
            }else {
                System.out.println("HiveMQ Connected Successfully .......");
            }
                   // publish();
                    subscribe();
        });





    }

    public static void subscribe(){

        mqtt5AsyncClient.subscribeWith()
                .topicFilter("partner_app_positions")
                .callback(publish -> {



           JSONObject partnerObject = new JSONObject(new String(publish.getPayloadAsBytes()));
                    System.out.println("Message Received " + partnerObject);
                    createPostgresConnection(partnerObject);

                })
                .send()
                .whenComplete((subAck,throwable)->{
                    if(throwable != null){
                        throwable.printStackTrace();
                        System.out.println("Subscribing to the topic failed ...");
                    }else {
                        System.out.println("Successfully Subscribed to the topic ...");
                    }

                });
    }




        
//     public static void publish(){
//        String myPayload = "My test";
//
//         mqtt5AsyncClient.publishWith()
//                 .topic("partner_app_positions")
//                 .payload(myPayload.getBytes())
//                 .retain(true)
//                 .qos(MqttQos.EXACTLY_ONCE)
//                 .send()
//                 .whenComplete((mqtt5Publish, throwable) -> {
//                     if (throwable != null) {
//                         System.out.println("Failed to publish to the topic...");
//                     } else {
//                         System.out.println("Published to the topic...");
//                     }
//
//                 });
//
//     }

}
