package com.sendyit.positions.store;

import com.sendyit.common.Main;
import com.sendyit.common.utils.Config;
import com.sendyit.positions.partnerLocation.PartnerData;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.json.JSONObject;
import org.postgis.PGgeometry;


public class Postgres {

    public static void createPostgresConnection(JSONObject partnerObject) {

        try {
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/parceltest");
            hikariConfig.setUsername("cliff");
            hikariConfig.setPassword("password");
            hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", "400");
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            hikariConfig.setAutoCommit(true);
            hikariConfig.setPoolName("positions_app");
            hikariConfig.setRegisterMbeans(true);
            hikariConfig.setMinimumIdle(5);
            hikariConfig.setMaximumPoolSize(10);
            HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);
            Jdbi dbi = Jdbi.create(hikariDataSource);
            dbi.installPlugin(new SqlObjectPlugin());


            org.postgis.Point pointToAdd = new org.postgis.Point();
            pointToAdd.setX(partnerObject.getDouble("lng"));
            pointToAdd.setY(partnerObject.getDouble("lat"));
            PGgeometry pGgeometry = new PGgeometry(pointToAdd);

            PartnerData position = new PartnerData();
             PartnerData.active =partnerObject.getBoolean("active");
             PartnerData.altitude = partnerObject.getDouble("altitude");
             PartnerData.busy= partnerObject.getBoolean("busy");
             PartnerData.carrierType= partnerObject.getInt("carrier_type");
             PartnerData.course= partnerObject.getDouble("course");
             PartnerData.deviceId=partnerObject.getInt("device_id");
             PartnerData.exclusiveStatus=partnerObject.getInt("exclusive_status");
             PartnerData.name=partnerObject.getString("name");
             PartnerData.partner_level=partnerObject.getInt("partner_level");
             PartnerData.phoneNo=partnerObject.getString("phone_no");
             PartnerData.rating=partnerObject.getDouble("rating");
             PartnerData.riderId=partnerObject.getInt("rider_id");
             PartnerData.riderStatus=partnerObject.getInt("rider_status");
             PartnerData.simCardsn=partnerObject.getString("sim_Card_sn");
             PartnerData.speed=partnerObject.getDouble("speed");
             PartnerData.vendorType=partnerObject.getInt("vendor_type");
             PartnerData.time=partnerObject.getString("time");
             PartnerData.lat_lng= Double.valueOf(pGgeometry.getValue());
             PartnerData.cityId= partnerObject.getInt("city_id");
             PartnerData.licenseStatus=partnerObject.getInt("licenseStatus");
             PartnerData.trackerImei= partnerObject.getString("tracker_imei");
             PartnerData.voltageLevel=partnerObject.getInt("voltageLevel");
             PartnerData.gsm=partnerObject.getInt("gsm");
             PartnerData.terminalInfo=partnerObject.getString("terminal_info");
             PartnerData.alarm=partnerObject.getString("alarm");
             PartnerData.language=partnerObject.getString("language");
             PartnerData.gpsStrength=partnerObject.getInt("gps_strength");
             PartnerData.gpsQuantity=partnerObject.getInt("gps_quantity");


                  System.out.println(position);



            dbi.useExtension(PositionsPgsql .class, ext -> ext.save(position));




        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
