package com.sendyit.positions.store;

public class Position {
    public static String name;
    public static Integer riderId;

    public static String getName() {
        return name;

    }

    public static Integer getRiderId() {
        return riderId;
    }

    public static void setName(String name) {
        Position.name = name;
    }

    public static void setRiderId(Integer riderId) {
        Position.riderId = riderId;
    }



}
