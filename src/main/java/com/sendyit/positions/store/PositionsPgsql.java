package com.sendyit.positions.store;

import com.sendyit.positions.partnerLocation.PartnerData;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindFields;
import org.jdbi.v3.sqlobject.locator.UseAnnotationSqlLocator;
import org.jdbi.v3.sqlobject.locator.UseClasspathSqlLocator;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

@UseClasspathSqlLocator
public interface PositionsPgsql {

    @SqlUpdate
    void save(@BindBean PartnerData position);
    //void save(@BindBean Position position);
}


