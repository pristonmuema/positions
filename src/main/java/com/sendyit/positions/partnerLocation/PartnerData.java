package com.sendyit.positions.partnerLocation;

public class PartnerData {
    public static String name;
    public static Integer riderId;
    public static Integer deviceId;
    public static Integer vendorType;
    public static Double speed;
    public static Integer carrierType;
    public static Double course;
    public static Double altitude;
    public static Integer cityId;
    public static Double lat_lng;
    public static Integer exclusiveStatus;
    public static Boolean busy;
    public static Integer partner_level;
    public static String simCardsn;
    public static String phoneNo;
    public static Boolean active;
    public static Double rating;
    public static Integer riderStatus;
    public static String time;
    public static Integer licenseStatus;
    public static String trackerImei;
    public static Integer voltageLevel;
    public static String terminalInfo;
    public static String alarm;
    public static String language;
    public static Integer gpsStrength;
    public static Integer gpsQuantity;
    public static Integer gsm;

    public static Double getRating() {
        return rating;
    }

    public static String getLanguage() {
        return language;
    }

    public static Integer getCityId() {
        return cityId;
    }

    public static String getName() {
        return name;
    }

    public static Integer getRiderId() {
        return riderId;
    }

    public static Integer getDeviceId() {
        return deviceId;
    }

    public static Integer getVendorType() {
        return vendorType;
    }

    public static Double getSpeed() {
        return speed;
    }

    public static Integer getCarrierType() {
        return carrierType;
    }

    public static Double getCourse() {
        return course;
    }

    public static Double getAltitude() {
        return altitude;
    }

    public static Double getLat_lng() {
        return lat_lng;
    }

    public static Integer getRiderStatus() {
        return riderStatus;
    }

    public static Integer getExclusiveStatus() {
        return exclusiveStatus;
    }

    public static Boolean getBusy() {
        return busy;
    }

    public static Integer getPartner_level() {
        return partner_level;
    }

    public static String getSimCardsn() {
        return simCardsn;
    }

    public static String getPhoneNo() {
        return phoneNo;
    }

    public static Boolean getActive() {
        return active;
    }

    public static String getTime() {
        return time;
    }

    public static Integer getLicenseStatus() {
        return licenseStatus;
    }

    public static String getTrackerImei() {
        return trackerImei;
    }

    public static Integer getVoltageLevel() {
        return voltageLevel;
    }

    public static String getTerminalInfo() {
        return terminalInfo;
    }

    public static String getAlarm() {
        return alarm;
    }

    public static Integer getGpsStrength() {
        return gpsStrength;
    }

    public static Integer getGpsQuantity() {
        return gpsQuantity;
    }

    public static Integer getGsm() {
        return gsm;
    }
}
